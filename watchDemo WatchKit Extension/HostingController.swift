//
//  HostingController.swift
//  watchDemo WatchKit Extension
//
//  Created by Prabhjinder Singh on 2019-10-15.
//  Copyright © 2019 Prabhjinder Singh. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
