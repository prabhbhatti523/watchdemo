//
//  NotificationView.swift
//  watchDemo WatchKit Extension
//
//  Created by Prabhjinder Singh on 2019-10-15.
//  Copyright © 2019 Prabhjinder Singh. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
#endif
