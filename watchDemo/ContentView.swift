//
//  ContentView.swift
//  watchDemo
//
//  Created by Prabhjinder Singh on 2019-10-15.
//  Copyright © 2019 Prabhjinder Singh. All rights reserved.
//

import SwiftUI

struct ContentView: View { 
    var body: some View {
        Text("Hello World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
